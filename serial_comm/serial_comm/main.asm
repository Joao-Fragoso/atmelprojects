;
; serial_comm.asm
;
; Created: 11/26/2018 8:40:06 PM
; Author : jlfragoso
;


; Replace with your application code
start:
	rcall usart_init
receive:
	lds r17, UCSR0A
	sbrs r17, RXC0
	rjmp receive
	lds r16, UDR0
	inc r16
send:
	lds r17, UCSR0A
	sbrs r17, UDRE0
	rjmp send
	sts UDR0, r16
    rjmp start


 usart_init:
	ldi r16, low(207)
	ldi r17, high(207)
	sts UBRR0H, r17
	sts UBRR0L, r16
	ldi r16, (1<<RXCIE0) | (1<<RXEN0) | (1<<TXEN0) 
	sts UCSR0B, r16
	ldi r16, (1<<USBS0) | (3<<UCSZ00)
	sts UCSR0C, r16
	ldi r16, (1<<U2X0)
	sts UCSR0A, r16
	ret