;
; MemoriaPrograma.asm
;
; Created: 9/2/2019 5:13:10 PM
; Author : jlfragoso
;


; Replace with your application code
start:
	ldi zl, low(dados)
	ldi zh, high(dados)

	clc
	rol zl
	rol zh ; multiplique 2 pra alinhar o end

	lpm r15, z+ ; leio e incrementa o z
	lpm r16, z+
	lpm r17, z

end:
	rjmp end

	.dseg  ; comeco a definir valores na RAM
dados:  ; define valores
;  fixos na memoria de programa
; memoria 16 bits (a de programa)
 .db 0xff, 0x05, 0x32, 0x00 ; alinha a memoria de novo
; a flash eh 16 bits (tem que ser par)