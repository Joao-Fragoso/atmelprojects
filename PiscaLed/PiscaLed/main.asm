; PiscaLed
; Created: 19/07/2017 00:50:54
; Author : Jo�o Leonardo Fragoso
;;#define XX R18
.equ A = 0xFF
start:
  ldi R16, A
  out DDRB, R16 ;
  ldi r16, 0b0100_0000
  out ddrd, r16
loop:
  sbi PORTB, 5
  sbi portd, 6
  rcall delay_05
  cbi PORTB, 5
  cbi portd, 6
  rcall delay_05
  rjmp loop

delay_05:
  ldi R16, 8
loop1:
  ldi R24, low(3037)
  ldi R25, high(3037)
delay_loop:
  adiw R24, 1
  brne delay_loop
  dec R16
  brne loop1
  ret
