;
; ADC.asm
;
; Created: 10/3/2017 1:28:53 PM
; Author : jlfragoso
;

#define LCD_4BITS
.cseg
.org 0x0000
;ISR Lists
	jmp start					; 0x0000 Reset
;	jmp INT0_ 					; 0x0002 IRQ0
;	jmp INT0_ 					; 0x0004 IRQ1
;	jmp PCINT1_					; 0x0006 PCINT0
;	jmp PCINT1_					; 0x0008 PCINT1
;	jmp PCINT2 					; 0x000A PCINT2
;	jmp WDT 					; 0x000C Estouro de Watchdog
;	jmp TIM2_COMPA 				; 0x000E Timer2 Comparacao A
;	jmp TIM2_COMPB 				; 0x0010 Timer2 Comparacao B
;	jmp TIM2_OVF 				; 0x0012 Timer2 Estouro
;	jmp TIM1_CAPT 				; 0x0014 Timer1 Captura
;	jmp TIM1_COMPA 				; 0x0016 Timer1 Comparacao A
;	jmp TIM1_COMPB 				; 0x0018 Timer1 Comparacao B
;	jmp TIM1_OVF 				; 0x001A Timer1 Estouro
;	jmp TIM0_COMPA 				; 0x001C Timer0 Comparacao A
;	jmp TIM0_COMPB 				; 0x001E Timer0 Comparacao B
;	jmp TIM0_OVF 				; 0x0020 Timer0 Estouro
;	jmp SPI_STC 				; 0x0022 SPI Transferencia Completa
;	jmp USART_RXC 				; 0x0024 USART RX Completa
;	jmp USART_EMPTY				; 0x0026 Registro de Dados Vazio na USART
;	jmp USART_TXC				; 0x0028 USART TX Completa
.org 0x002A
	jmp ADC_COMP                ; 0x002A ADC Conversion Complete
;	jmp EE_READY				; 0x002C EEPROM Ready
;	jmp ANALOG_COMP				; 0x002E Analog Comparator
;   jmp TWI						; 0x0030 2-wire Serial Interrupt (I2C)
;	jmp SPM_READY				; 0x0032 Store Program Memory Ready


.org INT_VECTORS_SIZE
.macro rolw
	clc
	rol @0
	rol @1
.endm

.include "lcd.inc"
; Replace with your application code
start:
	; configuracao da pilha
	ldi r16, high(RAMEND) 
	out sph, r16
	ldi r16, low(RAMEND)
	out spl, r16

	; configuracao das portas
	ldi r24, (1<<PD2); 	
	out ddrd, r24 ; port D botao e Led
	ldi r24, (1<<PB0)|(1<<PB1)|(1<<PB2)|(1<<PB3)|(1<<PB4)|(1<<PB5) ; port B LCD
	out ddrb, r24
	ldi r24, 0x00; port C todos entradas para analogicas
	out ddrc, r24
	ldi r24, 0xff ; disabling all digital inputs on C
	sts didr0, r24

	rcall init_lcd
	rcall print_init_message
	rcall wait_button
	rcall clear_lcd
	rcall print_message

	; configurando o adc
	ldi r24, (1 << REFS0) | (0 << ADLAR) | (0 << MUX0)
	sts admux, r24 ; slect AVcc ref, alinhado esquerda e ADC0 para conversao
;	ldi r24, 0x01
;	sts didr0, r24 ; desliga entrada digital PC0
	ldi r24, (0 << ACME) | (5 << ADTS0)
	sts adcsrb, r24 ; seleciona o comparacao B do contador 1 como inicio da conversao analogica
	;ldi r24, 0xaf
	;sts adcsra, r24; aden=1, adsc=0, adate=1, adif=0, adie=1, adps=111 adc on com /128 e autotrigger e interrupt habilitados

	;configuracao do contador 1 para gerar 4 overflows por segundo
	; fazer 4 amostras por segundo do adc 
	ldi r24, high(62499)
	sts ocr1ah, r24
	ldi r24, low(62499)
	sts ocr1al, r24
	ldi r24, high(62499)
	sts ocr1bh, r24
	ldi r24, low(62499)
	sts ocr1bl, r24
	ldi r24, 0x00
	sts tcnt1h, r24 ; zerando contador 1
	sts tcnt1l, r24 ; zerando contador 1
	sts timsk1, r24 ; disabling all interruption on conteur
	sts tccr1a, r24 ; no output generation and mode 0x4 (01_00)

	ldi r24, (0 << ICNC1) | (0 << ICES1) | (1 << WGM12) | (3 << CS10) ;0x0b ; mode 000_ mode 01 (0100) _prescaler 011 = /64
	sts tccr1b, r24 ; ligando pre-scaler 

	ldi r24, (1 << ADEN) | (1 << ADSC) | (1 << ADATE) | (0 << ADIF) | (1 << ADIE) | (7 << ADPS0)     ; 0xef
	sts adcsra, r24; aden=1, adsc=1, adate=1, adif=0, adie=1, adps=111 adc on com /128 e autotrigger e interrupt habilitados

start_measure:
	write_lcd_cmd (LCD_SET_CURSOR | LCD_LINE_0 | 0x04)
	rcall delay_2ms	
	clr r19 ; limpa semafaro
	ldi r24, 0xff
	out tifr1, r24; clear all flags

	sei ; habilita interrupcao
loop_esp: ; wait interruption
	tst r19
	breq loop_esp
	cli

	mov r12, r24 ; saving result for futher conversion
	mov r13, r25

	mov r28, r12
	mov r29, r13

	clc ; clear carry
	; multiply by 4
	rol r28
	rol r29
	clc
	rol r28
	rol r29

	
	; multiplying by 0.882...
	ldi r26, 226 ;(0.882...)
	mul r24, r26
	mov r24, r1 ; discarding less significant bits... r0
	mul r25, r26
	ldi r25,0x00
	add r24, r0
	adc r25, r1	
	add r24, r28
	adc r25, r29

	mov r10, r24
	mov r11, r25

	mov r24, r12
	mov r25, r13
conv_16bit_ascii:
	ldi r20, 5
begin_f:
	clr r26
	clr r27
begin_w :
	tst r25
	brne go_on
	tst r24
	brmi go_on ; avoid 2comp error
	cpi r24, 10
	brmi end_w
go_on:	
	sbiw r24, 10
	adiw r26, 1
	rjmp begin_w
end_w:
	ori r24, 0x30
	push r24
	movw r24, r26
	dec r20
	brne begin_f
end_conv:

	; mostra resultado
	pop r16
	write_lcd_data_reg r16
	pop r16
	write_lcd_data_reg r16
	pop r16
	write_lcd_data_reg r16
	pop r16
	write_lcd_data_reg r16
	pop r16
	write_lcd_data_reg r16





	write_lcd_cmd (LCD_SET_CURSOR | LCD_LINE_0 | 0x0A)
	rcall delay_2ms	
	mov r25, r11
	mov r24, r10 ; recovering data	
conv_16bit_ascii_2:
	ldi r20, 5
begin_f_2:
	clr r26
	clr r27
begin_w_2 :
	tst r25
	brne go_on_2
	tst r24
	brmi go_on_2 ; avoid 2comp error
	cpi r24, 10
	brmi end_w_2
go_on_2:	
	sbiw r24, 10
	adiw r26, 1
	rjmp begin_w_2
end_w_2:
	ori r24, 0x30
	push r24
	movw r24, r26
	dec r20
	brne begin_f_2
end_conv_2:

	; mostra resultado
	pop r16
	;write_lcd_data_reg r16
	pop r16
	write_lcd_data_reg r16
	write_lcd_data ','
	pop r16
	write_lcd_data_reg r16
	pop r16
	write_lcd_data_reg r16
	pop r16
	write_lcd_data_reg r16
	write_lcd_data 'V'

calc_bar:
/*	clc
	rol r12
	rol r13
	clc
	rol r12
	rol r13
	clc
	rol r12
	rol r13
*/
	rolw r12,r13
	rolw r12,r13
	rolw r12,r13

	ldi r24, 32
	mov r12, r24
	write_lcd_cmd (LCD_SET_CURSOR | LCD_LINE_1 | 0x00)
	rcall delay_2ms	
bar_loop:
	dec r12
	tst r12
	brmi next_measure
	cp r13, r12
	brmi print_p
	dec r12
	write_lcd_data 0xff
	rjmp bar_loop
print_p:
	dec r12
	write_lcd_data ' '
	rjmp bar_loop
next_measure:
    rjmp start_measure

print_init_message:
	ldi zl, low(_msg1)
	ldi zh, high(_msg1)
	rcall write_lcd_string_from_flash
	write_lcd_cmd (LCD_SET_CURSOR | LCD_LINE_1 | 0x00)
	ldi zl, low(_msg2)
	ldi zh, high(_msg2)
	rcall write_lcd_string_from_flash
	ret

print_message:
	ldi zl, low(_msg3)
	ldi zh, high(_msg3)
	rcall write_lcd_string_from_flash
	ret

wait_button:
	sbis PIND, PD3
	rjmp wait_button
	sbi PORTD, PD4
wait_release:
	sbic PIND, PD3
	rjmp wait_release
	cbi PORTD, PD4
	ret

ADC_COMP:
	; load result from counter
	lds r24, adcl
	lds r25, adch
	; set semaphore to go 
	ldi r19, 0x01
	reti

__constants:
_msg1: .db "ADC Tester",0,0
_msg2: .db "Press to Start",0,0
_msg3: .db "ADC=",0,0
__end_constants:
	nop