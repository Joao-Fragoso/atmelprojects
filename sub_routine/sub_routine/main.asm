;
; sub_routine.asm
;
; Created: 8/14/2017 1:08:42 PM
; Author : jlfragoso
;

.dseg
v3: .byte 10
v4: .byte 10
v1: .byte 10 
t1:
.dw 0
v2:
.db 2,4,6,7,8,10,12,14,16,18
t2: .dw 0
; Replace with your application code
.cseg
start:
	ldi R16, low(RAMEND)
	out SPL, R16
	ldi R16, high(RAMEND)
	out SPH, R16
	ldi R16, 0x33
	ldi R17, 0x25
	ldi R18, 0x0A
	push R16
	push R17
	pop R17
	push R18
	rcall rot
end : rjmp end
rot: in xl, spl
	in xh, sph
	adiw x,3
	ld r16, x+
	ld r17, x+
	ret
	ldi r16,0
	mov r0, r16
	ldi r16, low(v1)
	push r16
	ldi r16, high(v1)
	push r16
	ldi r16, 10
	push r16
	ldi r16, low(t1)
	push r16
	ldi r16, high(t1)
	push r16
	rcall sum_vect
    inc r16
    rjmp start

sum_vect:

	in xl, spl
	in xh, sph
	adiw x, 5
	ld r16, x+
	ld yh, x+
	ld yl, x
	ldi r18, 0
	ldi r19, 0
loop:
	ld r17, y+
	add r18, r17
	adc r19, r0
	dec r16
	brne loop 
	ret

