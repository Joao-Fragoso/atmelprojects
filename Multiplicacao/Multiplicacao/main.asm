; Multiplicacao.asm
; Created: 7/22/2017 6:34:26 PM
; Author : Joao Leonardo Fragoso

#define REG(X) R##X

.macro ldwi
	ldi @0, high(@2)
	ldi @1, low(@2)
.endm

.macro add_w
	add @1, @3
	adc @0, @2
.endm


start:
	add_w r1,r0,r3,r2
    ldi r16, 195
	ldi r17, 201

	ldwi r19, r18, 0

	ldi r18, 0
	ldi r19, 0
	ldi r20, 8

loop:
	clc
	ror r17
	brcc shift
	add r19, r16
shift:
	ror r19
	ror r18
	dec r20
	brne loop
end:
    rjmp end
