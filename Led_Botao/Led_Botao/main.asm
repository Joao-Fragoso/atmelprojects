;
; Led_Botao.asm
;
; Created: 7/23/2017 10:47:58 AM
; Author : Jo�o Leonardo Fragoso
.equ LED   = 6
.equ BOTAO = 2
start:
  ldi R16, 0xFF
  out DDRB, R16
  ldi R16, 0b0100_0000
  out DDRD, R16
  sbi PORTB, 5
botao_solto:
  sbis PIND, BOTAO
  rjmp botao_solto
  cbi PORTB, 5
botao_preso:
  sbic PIND, BOTAO
  rjmp botao_preso
  sbi PORTB, 5
loop:
  sbi PORTD, LED
  rcall delay_05
  cbi PORTD, LED
  rcall delay_05
  rjmp loop

delay_05:
  ldi R16, 8
loop1:
  ldi R24, low(3037)
  ldi R25, high(3037)
delay_loop:
  adiw R24, 1
  brne delay_loop
  dec R16
  brne loop1
  ret
