;
; Interruptions.asm
;
; Created: 8/29/2017 10:12:23 AM
; Author : jlfragoso
;
;ISR Lists
	jmp RESET					; Reset
	jmp INT0_ 					; IRQ0
	jmp INT0_ 					; IRQ1
	jmp PCINT1_					; PCINT0
	jmp PCINT1_					; PCINT1
;	jmp PCINT2 					; PCINT2
;	jmp WDT 					; Estouro de Watchdog
;	jmp TIM2_COMPA 				; Timer2 Comparacao A
;	jmp TIM2_COMPB 				; Timer2 Comparacao B
;	jmp TIM2_OVF 				; Timer2 Estouro
;	jmp TIM1_CAPT 				; Timer1 Captura
;	jmp TIM1_COMPA 				; Timer1 Comparacao A
;	jmp TIM1_COMPB 				; Timer1 Comparacao B
;	jmp TIM1_OVF 				; Timer1 Estouro
;	jmp TIM0_COMPA 				; Timer0 Comparacao A
;	jmp TIM0_COMPB 				; Timer0 Comparacao B
;	jmp TIM0_OVF 				; Timer0 Estouro
;	jmp SPI_STC 				; SPI Transferencia Completa
;	jmp USART_RXC 				; USART RX Completa


; Replace with your application code
RESET:
	sub r0, r0
	ldi r16, 0x10
	out DDRD, r16 ; configura PIND 5 para led
	ldi r16, 0x03
	sts EICRA, r16 ; configura INT0 para borda de subida
	ldi r16, 0x01
	out EIMSK, r16 ; configura mascara
	ldi  r16, 0x02
	sts PCICR, r16;
	ldi r16, 0x20
	sts PCMSK1, r16 ; habilita 5 bit PIND para interrupção
    ldi r16, 0x08 ; configura delay para 1/8 
	sei
start:
	tst R0
	breq start
	sbi PORTD, 5
	rcall delay_1_64
	cbi PORTD, 5
	rcall delay_1_64
    rjmp start

delay_1_64:
	mov r17, r16
loop1:
	ldi R24, low(3037)   ; 1 ciclo x 9 vezes				=         9
	ldi R25, high(3037)  ; 1 ciclo x 9 vezes				=         9
delay_loop:
	adiw R24, 1          ; 2 ciclos x (65536 – 3037) x 9		= 1.124.982
	brne delay_loop      ; (2 ciclos x (65536 – 3037) – 1) x 9	= 1.124.973
	dec R17	        ; 1 ciclo x 9 					=         9
	brne loop1		 ; 2 ciclos x 9 – 1				=        17
	ret			

INT0_:
	com r0
	pop r19
	pop r19
	ldi r19, low(start)
	push r19
	ldi r19, high(start)
	push r19
	reti
PCINT1_:
	sbrs r16, 5
	ldi r16, 0x04
	lsl r16
	pop r19
	pop r19
	ldi r19, low(start)
	push r19
	ldi r19, high(start)
	push r19
	reti