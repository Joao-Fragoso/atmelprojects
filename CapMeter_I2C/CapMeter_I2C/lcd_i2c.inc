/*
 * lcd_i2c.inc
 *
 *  Created: 10/17/2017 2:38:18 PM
 *   Author: jlfragoso
 */ 

 #ifndef __LCD_I2C_INC__
 #define __LCD_I2C_INC__


.equ LCD_ADDRESS		= (0x7c>>1)
.equ RGB_ADDRESS        = (0xc4>>1)
.equ LCD_POWER_UP		= 0x28
.equ LCD_8BIT_2LINES	= 0x38 
.equ LCD_8BIT_1LINE		= 0x30  ; power_up
.equ LCD_4BIT_2LINES	= 0x28 
.equ LCD_4BIT_1LINE		= 0x20 
.equ LCD_DON_COFF_BOFF	= 0x0C  ;--display on, cursor off, blink off
.equ LCD_DON_COFF_BON	= 0x0D  ;--display on, cursor off, blink on
.equ LCD_DON_CON_BOFF	= 0x0E  ;--display on, cursor on, blink off
.equ LCD_DON_CON_BON	= 0x0F  ;--display on, cursor on, blink on
.equ LCD_DOFF_COFF_BOFF	= 0x08  ;--display off, cursor off, blink off
.equ LCD_DOFF_COFF_BON	= 0x09  ;--display off, cursor off, blink on
.equ LCD_DOFF_CON_BOFF	= 0x0A  ;--display off, cursor on, blink off
.equ LCD_DOFF_CON_BON	= 0x0B  ;--display off, cursor on, blink on
.equ LCD_MODE_INC		= 0x06
.equ LCD_MODE_INC_SHIFT = 0x07
.equ LCD_MODE_DEC		= 0x04
.equ LCD_MODE_DEC_SHIF	= 0x05
.equ LCD_CLEAR_CODE  	= 0x01
.equ LCD_SET_CURSOR     = 0x80
.equ LCD_LINE_0         = 0x00
.equ LCD_LINE_1         = 0x40

.include "misc.inc"
.include "twi.inc"

.macro write_lcd_cmd
	ldi r24, LCD_ADDRESS
	ldi r25, 0x80
    ldi r26, @0
	rcall write_byte_twi
	delay_us 50
.endm

.macro write_lcd_data
	ldi r24, LCD_ADDRESS
	ldi r25, 0x40
	ldi r26, @0
	rcall write_byte_twi
	delay_us 50
.endm

.macro write_lcd_cmd_reg
	mov r26, @0
	ldi r24, LCD_ADDRESS
	ldi r25, 0x80
	rcall write_byte_twi
	delay_us 50	
.endm

.macro write_lcd_data_reg
	mov r26, @0
	ldi r24, LCD_ADDRESS
	ldi r25, 0x40
	rcall write_byte_twi
	delay_us 50	
.endm

.macro write_lcd_str_from_flash 
	ldi zh, high(@0)
	ldi zl, low(@0)
	rcall _write_lcd_string_from_flash
.endm

.macro write_lcd_str_from_ram 
	ldi zh, high(@0)
	ldi zl, low(@0)
	rcall _write_lcd_string_from_sram
.endm

.macro lcd_clear
	rcall _lcd_clear
.endm

.macro lcd_set_position
	write_lcd_cmd ((LCD_SET_CURSOR) | (@0<<6) | (@1))
.endm

.macro write_from_stack
	pop r26
	ldi r24, LCD_ADDRESS
	ldi r25, 0x40
	rcall write_byte_twi
	delay_us 50
.endm

.macro write_16bit_to_lcd
	ldi zh, high(conv_v)
	ldi zl, low(conv_v)
	rcall _write_lcd_string_from_sram
.endm

.cseg
init_lcd:
	write_lcd_cmd LCD_POWER_UP
	delay_6ms
	write_lcd_cmd LCD_POWER_UP
	delay_us 100
	write_lcd_cmd LCD_POWER_UP
	write_lcd_cmd LCD_POWER_UP
	write_lcd_cmd LCD_8BIT_2LINES
	write_lcd_cmd LCD_DOFF_COFF_BOFF
	write_lcd_cmd LCD_CLEAR_CODE
	rcall delay_2ms
	write_lcd_cmd LCD_MODE_INC
	write_lcd_cmd LCD_DON_COFF_BOFF

	ldi r24, RGB_ADDRESS
	ldi r25, 0x00
	ldi r26, 0x00
	rcall write_byte_twi
	ldi r24, RGB_ADDRESS
	ldi r25, 0x08
	ldi r26, 0xff
	rcall write_byte_twi
	ldi r24, RGB_ADDRESS
	ldi r25, 0x01
	ldi r26, 0x20
	rcall write_byte_twi

	ldi r24, RGB_ADDRESS
	ldi r25, 0x02
	ldi r26, 0x00
	rcall write_byte_twi
	ldi r24, RGB_ADDRESS
	ldi r25, 0x03
	ldi r26, 0x00
	rcall write_byte_twi
	ldi r24, RGB_ADDRESS
	ldi r25, 0x03
	ldi r26, 0xff
	rcall write_byte_twi

	ret

_lcd_clear:
	write_lcd_cmd LCD_CLEAR_CODE
	rcall delay_2ms
	ret	



_write_lcd_string_from_flash:
	clc
	rol zl
	rol zh
_loop_string:
	ldi r24, LCD_ADDRESS
	ldi r25, 0x40
	lpm r26, z+
	tst r26
	brne _p_string ; 0 end string
	ret
_p_string:
	rcall write_byte_twi
	delay_us 50
	rjmp _loop_string	

_write_lcd_string_from_sram:
	ldi r24, LCD_ADDRESS
	ldi r25, 0x40
	ld r26, z+
	tst r26
	breq _end_sram_string_ ; 0 end string
	rcall write_byte_twi
	delay_us 50
	rjmp _write_lcd_string_from_sram
_end_sram_string_:
	ret		


conv_16bit_ascii:
	ldi r20, 5
	ldi zh, high(conv_v+6)
	ldi zl, low(conv_v+6)
	sub r1,r1
	st -z, r1
begin_f:
	clr r26
	clr r27
begin_w :
	tst r25
	brne go_on
	tst r24
	brmi go_on ; avoid 2comp error
	cpi r24, 10
	brmi end_w
go_on:	
	sbiw r24, 10
	adiw r26, 1
	rjmp begin_w
end_w:
	ori r24, 0x30
	st -z, r24
	movw r24, r26
	dec r20
	brne begin_f
end_conv:
	; display result
	ret

.dseg
conv_v: .db 0,0,0,0,0,0

#endif



