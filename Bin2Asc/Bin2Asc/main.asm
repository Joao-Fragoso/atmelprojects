;
; Bin2Asc.asm
;
; Created: 9/27/2017 10:46:19 PM
; Author : jlfragoso
;


; Replace with your application code
start:
	ldi r25, high(value)
	ldi r24, low(value)
	ldi r23, high(12325)
	ldi r22, low(12325)
	rcall bit162asc


	ldi r24, 0xff ;low(12325)
	ldi r25, 0x7f ;high(12325)
conv_16bit_ascii:
	ldi r20, 5
begin_f:
	clr r26
	clr r27
begin_w :
	tst r25
	brne go_on
	tst r24
	brmi go_on ; avoid 2comp error
	cpi r24, 10
	brmi end_w
go_on:	
	sbiw r24, 10
	adiw r26, 1
	rjmp begin_w
end_w:
	ori r24, 0x30
	push r24
	movw r24, r26
	dec r20
	brne begin_f
end_conv:

	pop r16
	pop r16
	pop r16
	pop r16
	pop r16
fim_p:
    inc r16
    inc r16
    rjmp fim_p
; convert 16-bit value to a string
; r25:r24 ram address to store string result
; r23:r22 16-bit value

bit162asc:
	mov zl, r24
	mov zh, r25
	ldi r20, 5
l2:
	clr r24
	ldi r18, 16
	ldi r19, 10
l1:
	sec
	rol r22
	rol r23
	rol r24
	sub r24, r19
	brsh nl1
	andi r22, 0xfe
	add r24, r19
nl1:
	dec r18
	brne l1

	ori r24, 0x30
	push r24
	dec r20
	brne l2

	ldi r20, 5
l3:
	pop r24
	st z+,r24
	dec r20
	brne l3

	ldi r24, 0
	st z+, r24

	ret

.dseg
value: .byte 10