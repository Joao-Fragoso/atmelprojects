;
; InterruptBlinkLed.asm
;
; Created: 9/25/2018 1:56:04 PM
; Author : jlfragoso
;
.cseg
.org 0x0000
;ISR Lists
	jmp start					; 0x0000 Reset
	jmp INT0_ 					; 0x0002 IRQ0
;	jmp INT0_ 					; 0x0004 IRQ1
;	jmp PCINT1_					; 0x0006 PCINT0
;	jmp PCINT1_					; 0x0008 PCINT1
;	jmp PCINT2 					; 0x000A PCINT2
;	jmp WDT 					; 0x000C Estouro de Watchdog
;	jmp TIM2_COMPA 				; 0x000E Timer2 Comparacao A
;	jmp TIM2_COMPB 				; 0x0010 Timer2 Comparacao B
;	jmp TIM2_OVF 				; 0x0012 Timer2 Estouro
;	jmp TIM1_CAPT 				; 0x0014 Timer1 Captura
;	jmp TIM1_COMPA 				; 0x0016 Timer1 Comparacao A
;	jmp TIM1_COMPB 				; 0x0018 Timer1 Comparacao B
;	jmp TIM1_OVF 				; 0x001A Timer1 Estouro
.org 0x001c
	jmp TIM0_COMPA 				; 0x001C Timer0 Comparacao A
;	jmp TIM0_COMPB 				; 0x001E Timer0 Comparacao B
;	jmp TIM0_OVF 				; 0x0020 Timer0 Estouro
;	jmp SPI_STC 				; 0x0022 SPI Transferencia Completa
;	jmp USART_RXC 				; 0x0024 USART RX Completa
;	jmp USART_EMPTY				; 0x0026 Registro de Dados Vazio na USART
;	jmp USART_TXC				; 0x0028 USART TX Completa
;	jmp ADC_COMP                ; 0x002A ADC Conversion Complete
;	jmp EE_READY				; 0x002C EEPROM Ready
;	jmp ANALOG_COMP				; 0x002E Analog Comparator
;   jmp TWI						; 0x0030 2-wire Serial Interrupt (I2C)
;	jmp SPM_READY				; 0x0032 Store Program Memory Ready


.org INT_VECTORS_SIZE
.macro rolw
	clc
	rol @0
	rol @1
.endm

start:
	; configuracao da pilha
	ldi r16, high(RAMEND) 
	out sph, r16
	ldi r16, low(RAMEND)
	out spl, r16

	;zera r1
	ldi r16, 0
	mov r1, r16

	;coloca estado em 5 (0 --> 7)
	ldi r16, 5
	mov r7, r16
	; carrega o delay correspondente
	ldi zh, high(const_values+5)
	ldi zl, low(const_values+5)
	lpm r8, z

	;movendo valores pra RAM
	ldi r16, 8
	ldi zh, high(const_values)
	ldi zl, low(const_values)
	ldi xh, high(delays)
	ldi xl, low(delays)
load__ :
	lpm r17,z+
	st x+, r17
	dec r16
	brne load__


	; configuracao das portas
	ldi r16, (1<<PD5); 	
	out ddrd, r16 ; port D botao e Led

	;configurando int0 borda de subida...
	ldi r16, (3<<ISC00)
	sts EICRA, r16
	ldi r16, (1<<INT0)
	out EIMSK, r16



INT0_:
	inc r7
	andi r7, 0x07 ; mask
	breq off_led ;
	ldi r16, 0x07
	eor r7, r16
	breq on_led
	eor r7, r16
off_led:
	
	cbi PORTD, PD5

end_int0:
	reti

TIM0_COMPA:
	reti

const_values:
.db 0,8,15,31,63,125,250,255

.dseg
delays:
.byte 8
