;
; SevenSegments.asm
;
; Created: 9/9/2019 11:28:40 AM
; Author : jlfragoso
;
.cseg
.org 0x0000
;ISR Lists
	jmp start					; 0x0000 Reset
	jmp INT0_ 					; 0x0002 IRQ0
;	jmp INT1_ 					; 0x0004 IRQ1
;	jmp PCINT1_					; 0x0006 PCINT0
;	jmp PCINT1_					; 0x0008 PCINT1
;	jmp PCINT2 					; 0x000A PCINT2
;	jmp WDT 					; 0x000C Estouro de Watchdog
;	jmp TIM2_COMPA 				; 0x000E Timer2 Comparacao A
;	jmp TIM2_COMPB 				; 0x0010 Timer2 Comparacao B
;	jmp TIM2_OVF 				; 0x0012 Timer2 Estouro
;	jmp TIM1_CAPT 				; 0x0014 Timer1 Captura
;	jmp TIM1_COMPA 				; 0x0016 Timer1 Comparacao A
;	jmp TIM1_COMPB 				; 0x0018 Timer1 Comparacao B
;	jmp TIM1_OVF 				; 0x001A Timer1 Estouro
;	jmp TIM0_COMPA 				; 0x001C Timer0 Comparacao A
;	jmp TIM0_COMPB 				; 0x001E Timer0 Comparacao B
.ORG 0X0020
	jmp TIM0_OVF 				; 0x0020 Timer0 Estouro
;	jmp SPI_STC 				; 0x0022 SPI Transferencia Completa
;	jmp USART_RXC 				; 0x0024 USART RX Completa
;	jmp USART_EMPTY				; 0x0026 Registro de Dados Vazio na USART
;	jmp USART_TXC				; 0x0028 USART TX Completa
;	jmp ADC_COMP                ; 0x002A ADC Conversion Complete
;	jmp EE_READY				; 0x002C EEPROM Ready
;	jmp ANALOG_COMP				; 0x002E Analog Comparator
;   jmp TWI						; 0x0030 2-wire Serial Interrupt (I2C)
;	jmp SPM_READY				; 0x0032 Store Program Memory Ready


.org INT_VECTORS_SIZE

.include "seven_segment.inc"
.equ BUTTON = PIND
.equ BUTTON_BIT = 2
; Replace with your application code
start:
	; configuracao da pilha
	ldi r16, high(RAMEND) 
	out sph, r16
	ldi r16, low(RAMEND)
	out spl, r16

	;
	sbi DDRB, 5  ; PORTA DO LED PRA SAIDA
	sbi PORTB, 5 ; LIGANDO O LED

	ldi r16, 38
	mov r4, r16

	; configura o registrado
	ldi r16, 0x01
	sts TIMSK0, r16
	ldi r16, 0x00
	out TCCR0A, r16
	ldi r16, 0x5
	out TCCR0B, r16

	; configura a interrupcao INT0
	; por legibilidade	
	ldi r16, (1 << ISC01) | (1 << ISC00) ; coloca um nos bits certos ;;; + legivel 
	sts  EICRA, r16 ; nao �h in out ... 
	ldi r16, (1 << INT0)
	out EIMSK, r16

	; usar o R5 como flag de direcao da contagem
	ldi r16, 1
	mov r5, r16

	; zerando r1
	clr r1
	; inicializacao do display
	cbi DDRD, 2
	cbi PORTD,2

	rcall init_display
	clr r7
	ldi r16, 0xa
	mov r6, r16

	; terminou inicializacao
	; habilitar as interrupcoes

	sei

loop:
	mov r24, r7
	ldi r25, 0
	rcall display_value
;	wait_button_click BUTTON, BUTTON_BIT
	rcall delay_05
	add r7, r5
	brmi negative
	cp r7, r6
	brmi loop
	clr r7
	rjmp loop
negative:
	ldi r16, 9
	mov r7, r16
	rjmp loop

delay_05:
  ldi R16, 40
loop1:
  ldi R24, low(3037)
  ldi R25, high(3037)
delay_loop:
  adiw R24, 1
  brne delay_loop
  dec R16
  brne loop1
  ret

INT0_ :
	push r17
	push r18
	in r17, SREG
	neg r5
	out SREG, r17
	pop r18
	pop r17
	reti

TIM0_OVF:
	push r18
	in r18, SREG
	push r17
	push r16
	dec r4
	brne fim_ov
	ldi r16, 38
	mov r4, r16
	in r16, PORTB
	ldi r17, (1<<5)
	eor r16, r17
	out PORTB, r16
fim_ov:
	pop r16
	pop r17
	out SREG, r18
	pop r18
	reti