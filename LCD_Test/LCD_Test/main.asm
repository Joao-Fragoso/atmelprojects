;
; LCD_Test.asm
;
; Created: 9/11/2017 9:15:52 AM
; Author : jlfragoso
;

.equ LCD_CTRL = PORTB
.equ LCD_EN = PB0
.equ LCD_RS = PB1
.equ LCD_DATA = PORTD
.equ LCD_POWER_UP = 0x30
.equ LCD_8BIT_2LINES = 0x38 
.equ LCD_8BIT_1LINE  = 0x30  ; power_up
.equ LCD_4BIT_2LINES = 0x28 
.equ LCD_4BIT_1LINE  = 0x20 
.equ LCD_DON_COFF_BOFF = 0x0C  ;--display on, cursor off, blink off
.equ LCD_DON_COFF_BON = 0x0D  ;--display on, cursor off, blink on
.equ LCD_DON_CON_BOFF = 0x0E  ;--display on, cursor on, blink off
.equ LCD_DON_CON_BON = 0x0F  ;--display on, cursor on, blink on
.equ LCD_DOFF_COFF_BOFF = 0x08  ;--display off, cursor off, blink off
.equ LCD_DOFF_COFF_BON = 0x09  ;--display off, cursor off, blink on
.equ LCD_DOFF_CON_BOFF = 0x0A  ;--display off, cursor on, blink off
.equ LCD_DOFF_CON_BON = 0x0B  ;--display off, cursor on, blink on
.equ LCD_MODE_INC = 0x06
.equ LCD_MODE_INC_SHIFT = 0x07
.equ LCD_MODE_DEC = 0x04
.equ LCD_MODE_DEC_SHIF = 0x05
.equ LCD_CLEAR = 0x01

.macro delay_us
	ldi r24, @0
	rcall _delay_us
.endm

.macro delay_6ms
	rcall delay_2ms
	rcall delay_2ms
	rcall delay_2ms
.endm

.macro write_lcd_cmd
	ldi r24, @0
	rcall _write_lcd_cmd
	delay_us 50
.endm

.macro write_lcd_data
	ldi r24, @0
	rcall _write_lcd_data
	delay_us 50
.endm

; Replace with your application code
start:
	ldi r24,0xff
	out ddrd, r24
	ldi r24, 0x03
	out ddrb, r24
	rcall init_lcd
	write_lcd_data 'H'
	write_lcd_data 'e'
	write_lcd_data 'l'
	write_lcd_data 'l'
	write_lcd_data 'o'
	write_lcd_data ' '
	write_lcd_data '!'
	rcall _delay_us
fim:
    inc r16
    rjmp fim


init_lcd:
	write_lcd_cmd LCD_POWER_UP
	delay_6ms
	write_lcd_cmd LCD_POWER_UP
	write_lcd_cmd LCD_POWER_UP
	write_lcd_cmd LCD_8BIT_2LINES
	write_lcd_cmd LCD_DOFF_COFF_BOFF
	write_lcd_cmd LCD_CLEAR
	rcall delay_2ms
	write_lcd_cmd LCD_MODE_INC
	write_lcd_cmd LCD_DON_CON_BOFF
	ret


_write_lcd_cmd:   ; cmd on R24
	out LCD_DATA, R24 
    cbi LCD_CTRL, LCD_RS  ; RS = 0
	sbi LCD_CTRL, LCD_EN  ; EN = 1
	rcall delay_1us
	cbi LCD_CTRL, LCD_EN ; EN = 0
	ret

_write_lcd_data:   ; cmd on R24
	out LCD_DATA, R24 
    sbi LCD_CTRL, LCD_RS  ; RS = 1
	sbi LCD_CTRL, LCD_EN  ; EN = 1
	rcall delay_1us
	cbi LCD_CTRL, LCD_EN ; EN = 0
	ret

delay_1us:
	nop
	ldi r24, 2
loop1_d1:
	dec r24
	brne loop1_d1
	ret

_delay_us:  ; carregar em r24 valor em us
	ldi r25, 3
loop1_d45:
	nop
	dec r25
	brne loop1_d45
	dec r24
	brne _delay_us
	ret

delay_2ms:
	ldi r24, low(57520)
	ldi r25, high(57520)
loop1_d2:
	adiw r24,1
	brne loop1_d2
	ret