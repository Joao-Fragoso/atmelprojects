;
; PWM.asm
;
; Created: 9/5/2017 11:25:00 AM
; Author : jlfragoso
;


; Replace with your application code
start:
	ldi r17, 0x20
	ldi r16, 0x83
	out tccr0a, r16
	ldi r16, 0x05
	out tccr0b, r16
	ldi r16, 0xff
	out ocr0a, r16
	ldi r16, 0x40
	out ddrd, r16
	ldi r16, 0xff
	ldi r17, 0x00
	ldi r18, 0x32
	ldi r19, 0xdf
botao_solto:
  sbis PIND, 2
  rjmp botao_solto
botao_preso:
  sbic PIND, 2
  rjmp botao_preso
  and r17, r17
  breq diminui
aumenta:
  cp r19, r16
  brpl incrementa
  ldi r17, 0x00
  ldi r16, 0xff
  rjmp fim
incrementa:
  add r16, r18
  rjmp fim
diminui:
  cp r18, r16
  brmi decrementa
  ldi r17, 0x01
  ldi r16, 0x00
  rjmp fim
decrementa:
  sub r16, r18
  rjmp fim
fim:
 out ocr0a, r16
  rjmp botao_solto