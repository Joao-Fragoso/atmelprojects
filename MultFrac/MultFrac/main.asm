;
; MultFrac.asm
;
; Created: 10/4/2017 4:34:05 PM
; Author : jlfragoso
;


; Replace with your application code
start:
	ldi r24, low(1000)
	ldi r25, high(1000)
	mov r28, r24
	mov r29, r25
	clc ; clear carry
	; multiply by 4
	rol r28
	rol r29
	clc
	rol r28
	rol r29

	; multiplying by 0.882...
	ldi r26, 226 ;(0.882...)
	mul r24, r26
	mov r24, r1 ; discarding less significant bits... r0
	mul r25, r26
	ldi r25,0x00
	add r24, r0
	adc r25, r1

	add r24, r28
	adc r25, r29

    inc r16
    rjmp start
